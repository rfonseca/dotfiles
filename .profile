#
# .profile
#
# http://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html#Bash-Startup-Files
#
# .profile works for bash itself and when it's called as sh.
#
# We are using bash, so why not use an array?
# Disable sh checkers that fails when we use array and other sh related errors.
# shellcheck disable=SC3010,SC3030,SC3054

export EDITOR='emacsclient --tty'
export VISUAL='emacsclient --no-wait'
export PAGER='less'
export BROWSER='brave'
export GOPATH="$HOME/go"
export ISTIO_HOME="$HOME/.local/share/istio-1.10.0"
export QT_QPA_PLATFORMTHEME='qt5ct'
export CLOUDSDK_PYTHON_SITEPACKAGES='1'
export USE_GKE_GCLOUD_AUTH_PLUGIN=True

paths=(
    "$HOME/bin"
    "$GOPATH/bin"
    "$HOME/.emacs.d/bin"
    "$HOME/.local/bin"
    "$HOME/.cargo/bin"
    "$ISTIO_HOME/bin"
    "$HOME/.config/composer/vendor/bin"
    "$HOME/.google-cloud-sdk/bin"
    "$HOME/workspace/bitbucket.org/arquivei/winterfell/bin"
    "$PATH"
)

oldIFS="$IFS"
IFS=:
export PATH="${paths[*]}"
IFS="$oldIFS"
unset oldIFS

# Less
#
# Set the default Less options.
# Mouse-wheel scrolling has been disabled by -X (disable screen clearing).
# Remove -X and -F (exit if the content fits on one screen) to enable it.
export LESS='-F -g -i -M -R -S -w -X -z-4'
# Language

if [[ -z "$LANG" ]]; then
    export LANG='en_US.UTF-8'
fi

[[ -f ~/.bashrc ]] && . "$HOME/.bashrc"

# Start X if loging on TTY 1
if [[ -z "$DISPLAY" && "$XDG_VTNR" -eq 1 ]]; then
    exec startx "$HOME/.xinitrc" xmonad -- :1
fi
