#!/usr/bin/env bash
set -euo pipefail

if command -v optimus-manager 1>/dev/null 2>&1; then
    sudo /usr/bin/prime-switch
fi
