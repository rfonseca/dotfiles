#!/usr/bin/env bash
set -euo pipefail

# Inspiration:
# https://gist.github.com/intrntbrn/cdb590090a9fcd0d0d32ae24092ab42a

if [ "$@" ]; then
    xdg-open "$(echo "$@" | awk -F ' => ' '{print $2}')" 1>/dev/null 2>&1
else
    bookmarks=$(fd --extension sqlite bookmarks ~/.mozilla/firefox/)

    # bookmarks db is always locked when firefox is running
    # as a workaround, we can just create a copy of the db
    orig_sha1=$(sha1sum "${bookmarks}" | awk '{print $1}')
    copy_sha1=$(sha1sum "${bookmarks}.copy" 2>/dev/null | awk '{print $1}')

    # only create a new copy if the db has been modified
    if [ "${orig_sha1}" != "${copy_sha1}" ]; then
        cp -f "${bookmarks}" "${bookmarks}.copy"
    fi

    # fetch all bookmark records from copy, show menu and open selected bookmark in browser
    sqlite3 -separator " => " "${bookmarks}.copy" ' SELECT i.title, u.url FROM items i JOIN urls u ON (i.urlId = u.id)'
    jq -r '.roots[].children[] | select(.type == "url") | .name + " => " + .url' ~/.config/BraveSoftware/Brave-Browser/Default/Bookmarks

fi
