#!/bin/bash
# shellcheck disable=SC2039

# TODO: Identify and remove unused binaries
BINARIES=(
    #sigs.k8s.io/kind
    github.com/aarzilli/gdlv
    github.com/cweill/gotests/gotests
    github.com/fatih/gomodifytags
    github.com/fzipp/gocyclo/cmd/gocyclo
    github.com/go-delve/delve/cmd/dlv
    github.com/ofabry/go-callvis
    github.com/segmentio/topicctl/cmd/topicctl
    github.com/stamblerre/gocode
    github.com/x-motemen/gore/cmd/gore
    golang.org/x/tools/cmd/godoc
    golang.org/x/tools/cmd/goimports
    golang.org/x/tools/cmd/gorename
    golang.org/x/tools/cmd/guru
    golang.org/x/tools/cmd/present
    golang.org/x/tools/gopls
)
for BIN in "${BINARIES[@]}"; do
    echo "----- Getting ${BIN} -----"
    GO111MODULE="on" go install -v "${BIN}@latest"
done
