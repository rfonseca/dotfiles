#!/usr/bin/env sh

DEV=$(xinput list | grep Touchpad |  grep -o -E 'id=[0-9]+' | cut -d= -f2)

PROP=$(xinput list-props ${DEV} | grep -o -E 'Natural Scrolling Enabled \([0-9]+' | grep -o -E '[0-9]+')

xinput set-prop ${DEV} ${PROP} 1
