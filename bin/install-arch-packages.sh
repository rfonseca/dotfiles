#!/usr/bin/env bash

set -euo pipefail

echo "Ensure yay is installed..."
if [[ -z "$(command -v yay 2>/dev/null)" ]]; then
    sudo pacman --quiet --sync --refresh --needed git base-devel curl openssl
    git clone https://aur.archlinux.org/yay.git
    (cd yay && makepkg -si)
    rm -rf yay
fi

echo "Installing packages..."
yay --quiet --sync --needed \
    xdg-user-dirs \
    wget unzip \
    autorandr feh picom \
    dunst xss-lock i3lock-color \
    trayer copyq network-manager-applet \
    xorg-{server,xinit,xinput,setxkbmap,xrandr,xprop,xkill,xbacklight} \
    xmonad{,-contrib} xmobar \
    kitty \
    emacs-git neovim code \
    zsh{,-completions,-autosuggestions,-syntax-highlighting,-theme-powerlevel10k} \
    fzf fd ripgrep \
    go gopls \
    nerd-fonts-{fira-code,hack} awesome-terminal-fonts
