#!/usr/bin/env bash

SOURCED=false && [ "$0" = "$BASH_SOURCE" ] || SOURCED=true

if ! $SOURCED; then
  # Enable strict mode
  set -euo pipefail
  IFS=$'\n\t'
fi

WALLPAPER_DIR=${HOME}/Pictures/wallpapers
CONNECTED_MONITORS=$(xrandr | grep -w -o connected | wc -l)
CMD="feh --recursive --randomize --bg-fill ${WALLPAPER_DIR}"

for i in $(seq 2 "${CONNECTED_MONITORS}");do
    CMD="$CMD ${WALLPAPER_DIR}"
done

eval "${CMD}"
