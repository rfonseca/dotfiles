#!/usr/bin/env bash
set -euo pipefail

if [ "$@" ]; then
    #echo "$@" | awk '{print $1}' | xargs bt activate
    bt activate "$(echo "$@" | awk '{print $1}')"
else
    #active_window=$(bt active | awk '{print $1}')
    bt list
fi
