#!/bin/sh

# More information at:
#   https://wiki.archlinux.org/index.php/Udisks
# Install udisks2
#   pacman -S udisks2

# Device will be mounted on /run/media/${USER}/${DEVNAME}

pathtoname() {
    udevadm info -p /sys/"$1" | awk -v FS== '/DEVNAME/ {print $2}'
}

stdbuf -oL -- udevadm monitor --udev -s block | while read -r -- _ _ event devpath _; do
        if [ "$event" = add ]; then
            devname=$(pathtoname "$devpath")
            udisksctl mount --block-device "$devname" --no-user-interaction
        fi
done
