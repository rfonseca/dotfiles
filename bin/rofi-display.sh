#!/bin/bash

# TODO: check ROFI_RETV, if not set run rofi rofi: -modi display:rofi-display.sh -show display
#echo -en "ROFI_RETV=$ROFI_RETV\0nonselectable\x1ftrue\n"
# https://github.com/davatorium/rofi/blob/next/doc/rofi-script.5.markdown

PROFILE=$*
PRIMARY=$(xrandr | grep ' connected ' | awk '{ print$1 }' | head -1)
SECONDARY=$(xrandr | grep ' connected ' | awk '{ print$1 }' | tail -1)

monitors=$(xrandr | grep ' connected ' | awk '{ print$1 }' | wc -l)

if [[ "$monitors" -eq 1 ]]; then
    DISCONNECTED=' (disconnected)\0nonselectable\x1ftrue'
else
    DISCONNECTED=
fi

echo -en "\0prompt\x1fChoose display\n"
echo -en "\0no-custom\x1ftrue\n"
#TODO: Learn about markup
#echo -en "\0markup-rows\x1ftrue\n"

if [ -z "$PROFILE" ]; then
    echo "Notebook"
    echo -en "Workstation${DISCONNECTED}\n"
    echo -en "WorkstationH${DISCONNECTED}\n"
    echo -en "Presentation${DISCONNECTED}\n"
    echo -en "Mirrored${DISCONNECTED}\n"
else
    case "$PROFILE" in
        Notebook)
            xrandr \
                --output "$PRIMARY" --primary --auto \
                --output "$SECONDARY" --off
            ;;
        Workstation)
            xrandr \
                --output "$PRIMARY" --primary --auto \
                --output "$SECONDARY" --auto --right-of "$PRIMARY" \
                --rotate right
            ;;
        WorkstationH)
            xrandr \
                --output "$PRIMARY" --primary --auto \
                --output "$SECONDARY" --auto --right-of "$PRIMARY"
            ;;
        Presentation)
            xrandr \
                --output "$PRIMARY" --primary --auto \
                --output "$SECONDARY" --auto --right-of "$PRIMARY"
            ;;
        Mirrored)
            xrandr \
                --output "$PRIMARY" --primary --auto \
                --output "$SECONDARY" --auto --right-of "$PRIMARY" \
                --same-as "$PRIMARY"
            ;;
        *)
            notify-send \
                --expire-time=5000 \
                --icon=dialog-error \
                "Cannot activate display profile $PROFILE"
            exit 1
            ;;
    esac

    # Restore or set the wallpaper
    if [ -f ~/.fehbg ]; then
        ~/.fehbg
    fi

    notify-send --expire-time=5000 --icon=video-display "Activated display profile $PROFILE"
fi
