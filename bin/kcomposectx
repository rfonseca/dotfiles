#!/usr/bin/env bash

# Bash strict mode
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

function check-program {
    if ! command -v "$1" &>/dev/null; then
        echo "$1 could not be found"
        echo "See: $2"
        exit 1
    fi
}

check-program fd https://github.com/sharkdp/fd
check-program fzf https://github.com/junegunn/fzf

config_dir=${XDG_CONFIG_HOME:-$HOME/.config}/kcompose
active_kcompose_profile=$HOME/.kcompose

current=$(basename "$(readlink -f "${active_kcompose_profile}")")
target=$(
    fd --base-directory "${config_dir}/profiles" --strip-cwd-prefix --max-depth=1 --type=d |
        tr -d '/' |
        fzf --no-multi --select-1 --exit-0 \
            --prompt "Select kcompose context (current is $current):"
)

if [[ ${current} == "${target}" ]]; then
    echo "Profile ${target} is already active."
    exit 0
fi

echo "Switching to $(basename "$target")."
ln -snf "${config_dir}/profiles/$target" "${active_kcompose_profile}"
