#!/usr/bin/env bash

current_tty=$(tty 2>/dev/null)
[[ $current_tty != /dev/tty* ]] && exit
current_tty=${current_tty#/dev/tty}

PS3='Please enter your choice: '
options=("spectrwm" "bspwm" "xmonad" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "spectrwm") break;;
        "bspwm") break;;
        "xmonad") break;;
        "Quit") exit;;
        *) echo "invalid option $REPLY";;
    esac
done

startx ~/.xinitrc "$opt"  -- ":${current_tty:-0}"
