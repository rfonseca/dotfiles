-- My xmonad configuration
------------------------------------------------------------------------
-- IMPORTS

------------------------------------------------------------------------
-- https://gitlab.com/dwt1/dotfiles/-/blob/master/.xmonad/xmonad.hs

-- Thanks to DistroTube

-- Base

-- Data
import Data.Monoid
import System.Exit (exitSuccess)
import XMonad
  ( Default (def),
    KeyMask,
    KeySym,
    Query,
    WindowSet,
    X,
    XConfig
      ( XConfig,
        focusedBorderColor,
        layoutHook,
        manageHook,
        modMask,
        normalBorderColor,
        startupHook,
        terminal,
        workspaces
      ),
    className,
    composeAll,
    doFloat,
    io,
    mod4Mask,
    resource,
    sendMessage,
    spawn,
    title,
    xK_b,
    xmonad,
    (-->),
    (<&&>),
    (<+>),
    (<||>),
    (=?),
  )
import XMonad.Config.Desktop
import XMonad.Config.Kde
-- Utilities

-- Layouts modifiers

-- Hooks
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
  ( avoidStruts,
    manageDocks,
  )
import XMonad.Hooks.ManageHelpers
  ( doFullFloat,
    isFullscreen,
  )
import XMonad.Hooks.SetWMName
import XMonad.Layout.MultiToggle
  ( EOT (EOT),
    mkToggle,
    (??),
  )
import qualified XMonad.Layout.MultiToggle as MT
  ( Toggle (..),
  )
import XMonad.Layout.MultiToggle.Instances
  ( StdTransformers
      ( NBFULL,
        NOBORDERS
      ),
  )
import XMonad.Layout.NoBorders
import qualified XMonad.StackSet as W
import XMonad.Util.EZConfig
import XMonad.Util.NamedScratchpad
import XMonad.Util.SpawnOnce (spawnOnce)

------------------------------------------------------------------------
-- MAIN
------------------------------------------------------------------------
main :: IO ()
main = xmonad =<< statusBar barCmd barPP barToogle baseConfig

-- baseConfig :: XConfig(XMonad.Layout.LayoutModifier.ModifiedLayout XMonad.Hooks.ManageDocks.AvoidStruts (Choose Tall (Choose (Mirror Tall) Full)))
baseConfig =
  ewmh
    kdeConfig
      { manageHook =
          manageDocks
            <+> (isFullscreen --> doFullFloat)
            <+> myManageHook
            <+> manageHook desktopConfig,
        startupHook = myStartupHook,
        layoutHook = myLayoutHook,
        modMask = mod4Mask,
        workspaces = ["1", "2", "3", "4", "5", "6"],
        terminal = "kitty",
        normalBorderColor = nord !! 1,
        focusedBorderColor = nord !! 8
      }
    `additionalKeysP` myKeybindings

------------------------------------------------------------------------
-- Keybindings
------------------------------------------------------------------------

-- use xev to check key names
myKeybindings :: [([Char], X ())]
myKeybindings =
  -- Xmonad
  [ ("M-C-r", spawn "xmonad --recompile"), -- Recompiles xmonad
    ("M-S-r", spawn "xmonad --restart"), -- Restarts xmonad
    ("M-S-q", io exitSuccess), -- Quits xmonad
    ("M-S-<Delete>", spawn "lock-screen.sh"),
    ("M-S-C-<Delete>", spawn "systemctl suspend"),
    -- Menu
    ("M-p", spawn "rofi -show run"),
    ("M1-<Tab>", spawn "rofi -show window"),
    ("M-d", spawn "rofi -modi display:rofi-display.sh -show display"),
    ( "M-<Tab>",
      spawn "rofi -modi switch-tab:rofi-switch-tab.sh -show switch-tab"
    ),
    ( "M-S-<Tab>",
      spawn "rofi -modi switch-tab:rofi-open-bookmark.sh -show switch-tab"
    ),
    -- Layouts
    -- Reminder:
    --  mod-{w,e,r} switch focus to screens {1,2,3}
    --  mod-<space> cycles layouts
    ("M-m", sendMessage $ MT.Toggle NBFULL), -- Monocle mone (fullscreen, no borders)
    -- Scratchpads
    ("M-<F1>", namedScratchpadAction myScratchPads "terminal"),
    ("M-<F2>", namedScratchpadAction myScratchPads "slack"),
    ("M-<F3>", namedScratchpadAction myScratchPads "monitor"),
    -- Media
    ("<XF86AudioPlay>", spawn "playerctl play-pause"),
    ("<XF86AudioPrev>", spawn "playerctl previous"),
    ("<XF86AudioNext>", spawn "playerctl next"),
    -- Audio
    ("<XF86AudioMute>", spawn "amixer set Master toggle"),
    ("<XF86AudioLowerVolume>", spawn "amixer set Master 5%- unmute"),
    ("<XF86AudioRaiseVolume>", spawn "amixer set Master 5%+ unmute"),
    -- Mic
    ("S-<XF86AudioMute>", spawn "amixer set Capture toggle"),
    ("S-<XF86AudioLowerVolume>", spawn "amixer set Capture 1%- unmute"),
    ("S-<XF86AudioRaiseVolume>", spawn "amixer set Capture 1%+ unmute"),
    -- Backlight
    ("<XF86MonBrightnessUp>", spawn "xbacklight +5"),
    ("<XF86MonBrightnessDown>", spawn "xbacklight -5"),
    -- Misc
    ( "<XF86Calculator>",
      spawn "rofi -show calc -modi calc -no-show-match -no-sort"
    ),
    --  , ("<Print>", spawn "xfce4-screenshooter")
    ("<Print>", spawn "flameshot gui"),
    ("M-v", spawn "copyq show"),
    ("M-S-v", spawn "paste-env")
  ]

------------------------------------------------------------------------
-- Status bar (xmobar)
------------------------------------------------------------------------

barCmd :: [Char]
barCmd = "xmobar $HOME/.xmonad/xmobarrc"

barToogle :: XConfig l -> (KeyMask, KeySym)
barToogle XConfig {XMonad.modMask = modMask} = (modMask, xK_b)

barPP :: PP
barPP =
  xmobarPP
    { ppOrder = \(ws : _ : _ : _) -> [ws],
      -- , ppCurrent         = xmobarColor (nord!! 7)  (nord!!1) . \s -> "●"
      ppCurrent = xmobarColor (nord !! 7) (nord !! 1) . const "\xf192 ", -- 
      ppUrgent = xmobarColor (nord !! 11) (nord !! 1) . const "\xf06a ", -- 
      ppVisible = xmobarColor (nord !! 7) (nord !! 1) . const "\xf62e ", -- 
      ppHidden = xmobarColor (nord !! 10) (nord !! 1) . const "\xf62e ", -- 
      ppHiddenNoWindows = xmobarColor (nord !! 10) (nord !! 1) . const "\xf62f ", -- 
      --                , ppTitle           = xmobarColor nord7     nord!!1
      ppOutput = putStrLn,
      ppWsSep = "",
      ppSep = "  "
    }

------------------------------------------------------------------------
-- Nord Theme
------------------------------------------------------------------------

-- https://www.nordtheme.com/docs/colors-and-palettes
nord :: [[Char]]
nord =
  [ "#2e3440", -- nord  0
    "#3b4252", -- nord  1
    "#434c5e", -- nord  2
    "#4c566a", -- nord  3
    "#d8dee9", -- nord  4
    "#e5e9f0", -- nord  5
    "#eceff4", -- nord  6
    "#8fbcbb", -- nord  7
    "#88c0d0", -- nord  8
    "#81a1c1", -- nord  9
    "#5e81ac", -- nord 10
    "#bf616a", -- nord 11
    "#d08770", -- nord 12
    "#ebcb8b", -- nord 13
    "#a3be8c", -- nord 14
    "#b48ead"
  ] -- nord 15

------------------------------------------------------------------------
-- SCRATCHPADS
------------------------------------------------------------------------

myScratchPads :: [NamedScratchpad]
myScratchPads =
  [ NS
      "terminal"
      "kitty --class scratchpad --instance-group scratchpad --single-instance"
      (className =? "scratchpad")
      manageTerm,
    NS
      "monitor"
      "kitty --class monitor --instance-group monitor --single-instance btop"
      (className =? "monitor")
      manageTerm,
    NS
      "slack"
      "slack"
      (className =? "Slack")
      manageTerm
  ]
  where
    manageTerm = customFloating $ W.RationalRect l t w h
      where
        h = 0.9
        w = 0.9
        t = 0.95 - h
        l = 0.95 - w

------------------------------------------------------------------------
-- MANAGEHOOK
------------------------------------------------------------------------
-- Sets some rules for certain programs. Examples include forcing certain
-- programs to always float, or to always appear on a certain workspace.
-- Forcing programs to a certain workspace with a doShift requires xdotool
-- if you are using clickable workspaces. You need the className or title
-- of the program. Use xprop to get this info.

myManageHook :: Query (Data.Monoid.Endo WindowSet)
myManageHook =
  composeAll
    -- using 'doShift ( myWorkspaces !! 7)' sends program to workspace 8!
    -- I'm doing it this way because otherwise I would have to write out
    -- the full name of my clickable workspaces, which would look like:
    -- doShift "<action xdotool super+8>gfx</action>"
    [ (title =? "Picture in picture" <||> title =? "Picture-in-Picture")
        --> doFloat,
      (className =? "firefox" <&&> resource =? "Dialog") --> doFloat,
      (className =? "copyq") --> doFloat,
      (className =? "Peek") --> doFloat,
      (className =? "zoom" <&&> title =? "Chat") --> doFloat,
      (className =? "Xfce4-screenshooter") --> doFloat,
      (className =? "nm-connection-editor") --> doFloat
    ]
    <+> namedScratchpadManageHook myScratchPads

------------------------------------------------------------------------
-- AUTOSTART
------------------------------------------------------------------------

myStartupHook :: X ()
myStartupHook = do
  spawnOnce "/usr/lib/xfce4/notifyd/xfce4-notifyd &"
  -- Tray
  spawnOnce "~/.xmonad/tray.sh &"
  spawnOnce "/usr/bin/nm-applet &"
  spawnOnce "/usr/bin/copyq &"
  spawnOnce "/usr/bin/flameshot &"
  spawnOnce "/usr/bin/gromit-mpx &"
  -- Common apps
  -- spawnOnce "/usr/bin/firefox &"
  spawnOnce "/usr/bin/autorandr --change &"
  -- spawnOnce "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &"
  spawnOnce "/usr/bin/screenkey --start-disabled &"
  spawnOnce "/usr/bin/blueman-applet &"
  -- spawnOnce "/usr/bin/lxsession &"
  -- "xmonad and Java swing application's don't play well with the default config.
  -- You'll need to set your WM name to "LG3D" for Java Swing apps to work."
  -- Source: https://bbs.archlinux.org/viewtopic.php?id=95437
  setWMName "LG3D"

------------------------------------------------------------------------
-- LAYOUTS
------------------------------------------------------------------------

myLayoutHook =
  avoidStruts $
    smartBorders $
      mkToggle (NBFULL ?? NOBORDERS ?? EOT) $
        layoutHook def
