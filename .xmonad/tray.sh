#!/usr/bin/env bash

killall trayer 2>/dev/null

/usr/bin/trayer \
    --edge top \
    --align right \
    --widthtype percent \
    --padding 6 \
    --SetDockType true \
    --SetPartialStrut true \
    --expand false \
    --monitor "primary" \
    --transparent true \
    --alpha 0 \
    --width 10 \
    --tint '0x3b4252' \
    --height 30
