#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='/usr/bin/lsd'
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

# Interactive shell
if [[ "$-" == *i* ]]; then
    # Setup autocomplete
    if [[ "$TERM" == xterm-kitty ]]; then
        # shellcheck disable=SC1090
        source <(kitty + complete setup bash)
    fi
    # Setup prompt
    if [[ -x "$(command -v starship)" ]]; then
        eval "$(starship init bash)"
    else
        # TODO: Maybe make this a little cooler
        PS1='[\u@\h \W]\$ '
    fi
fi
