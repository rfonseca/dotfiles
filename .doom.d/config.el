;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Rodrigo J. da Fonseca"
      user-mail-address "rodrigo.fonseca@arquivei.com.br")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))
;; Hack Nerd Font Mono
;;
;; (setq doom-font (font-spec :family "Hack Nerd Font Mono" :size 15)
;;       doom-variable-pitch-font (font-spec :family "Hack Nerd Font" :size 15))
;; (setq doom-font "Hack Nerd Font Mono-13"
;;       doom-variable-pitch-font "Hack Nerd Font-13")
;; (setq doom-font (font-spec :family "Hack Nerd Font Mono" :size 16)
;;       doom-variable-pitch-font (font-spec :family "Hack Nerd Font" :size 16)
;;       doom-big-font (font-spec :family "Hack Nerd Font Mono" :size 26))
(setq doom-font (font-spec :family "FiraCode Nerd Font Mono" :size 15)
      doom-variable-pitch-font (font-spec :family "Nimbus Sans" :size 15)
      doom-big-font (font-spec :family "FiraCode Nerd Font Mono" :size 24))
(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-nord)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/Documents/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; Performance improving
;; https://github.com/emacs-lsp/lsp-mode
;(setq read-process-output-max (* 3 1024 1024)) ;; 3mb
(setq read-process-output-max (* 1024 1024)) ;; 3mb
;;(setq lsp-enable-file-watchers 't);;
(setq lsp-file-watch-threshold 5000)

(setq shell-file-name "fish")

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; Because (format +onsave) and +lsp are both active
;; Golang formmating if handled by lsp-format-buffer (in this case gopls)
;; From the official documentation at https://github.com/golang/tools/blob/master/gopls/doc/emacs.md
(after! go-mode
  ;; (set-lookup-handlers! 'go-mode
  ;;   ;; :definition #'lsp-find-definition
  ;;   :definition #'lsp-ui-peek-find-references
  ;;   :references #'lsp-find-references
  ;;   :documentation #'godoc-at-point)
  ;; Set up before-save hooks to format buffer and add/delete imports.
  ;; Make sure you don't have other gofmt/goimports hooks enabled.
  (defun lsp-go-install-save-hooks ()
    (add-hook 'before-save-hook #'lsp-format-buffer t t)
    (add-hook 'before-save-hook #'lsp-organize-imports t t))
  (add-hook 'go-mode-hook #'lsp-go-install-save-hooks)
  (with-eval-after-load 'lsp-mode
    (add-to-list 'lsp-file-watch-ignored-directories "[/\\\\]\\.cover\\'")
    (add-to-list 'lsp-file-watch-ignored-directories "[/\\\\]\\.cache\\'")
    (add-to-list 'lsp-file-watch-ignored-directories "[/\\\\]\\vendor\\'"))
  )

;; (use-package! langtool
;;   :commands (langtool-check
;;              langtool-check-done
;;              langtool-show-message-at-point
;;              langtool-correct-buffer)
;;   :init (setq langtool-default-language "en-US")
;;   :config
;;   (setq langtool-java-classpath
;;         "/usr/share/languagetool:/usr/share/java/languagetool/*"))

(evil-ex-define-cmd "W" #'evil-write)

(setq projectile-project-search-path '("~/go/src/bitbucket.org/arquivei/"
                                       "~/go/src/bitbucket.org/rfonseca/"
                                       "~/go/src/github.com/arquivei/"
                                       "~/go/src/github.com/rfonseca/"
                                       "~/go/src/gitlab.com/arquivei/"
                                       "~/go/src/gitlab.com/rfonseca/"
                                       "~/workspace/arquivei/"
                                       "~/workspace/"))

(add-hook! 'elfeed-search-mode-hook 'elfeed-update)

(setq org-roam-directory "~/Documents/roam/notes")

;(use-package org-roam-server
;  :hook
;  (after-init . org-roam-server-mode)
;  :config
;  (setq org-roam-server-host "127.0.0.1"
;        org-roam-server-port 8888
;        org-roam-server-authenticate nil
;        org-roam-server-export-inline-images t
;        org-roam-server-serve-files nil
;        org-roam-server-served-file-extensions '("pdf" "mp4" "ogv")
;        org-roam-server-network-poll t
;        org-roam-server-network-arrows nil
;        org-roam-server-network-label-truncate t
;        org-roam-server-network-label-truncate-length 60
;        org-roam-server-network-label-wrap-length 20))
;
(use-package! websocket
    :after org-roam)

(use-package! org-roam-ui
    :after org-roam ;; or :after org
;;         normally we'd recommend hooking orui after org-roam, but since org-roam does not have
;;         a hookable mode anymore, you're advised to pick something yourself
;;         if you don't care about startup time, use
;;  :hook (after-init . org-roam-ui-mode)
    :config
    (setq org-roam-ui-sync-theme t
          org-roam-ui-follow t
          org-roam-ui-update-on-save t
          org-roam-ui-open-on-start t))

(add-hook 'dap-stopped-hook
          (lambda (arg) (call-interactively #'dap-hydra)))

;;; config.el ends here
