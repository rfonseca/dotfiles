# My ZSH configuration
#
# References:
# https://wiki.archlinux.org/index.php/Zsh


# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block, everything else may go below.
#if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
#  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
#fi

autoload -Uz compinit promptinit
compinit
promptinit

# Set vi mode
bindkey -v

#http://zsh.sourceforge.net/Doc/Release/Options.html
## Changing Directories
setopt AUTO_CD
setopt AUTO_PUSHD
#setopt CDABLE_VAR
setopt CHASE_DOTS
setopt CHASE_LINKS
setopt PUSHD_IGNORE_DUPS
## Completion

## History
#set history size
export HISTSIZE=10000
#save history after logout
export SAVEHIST=10000
#history file
export HISTFILE=~/.zhistory
#append into history file
setopt INC_APPEND_HISTORY
#save only one command if 2 common are same and consistent
setopt HIST_IGNORE_DUPS
#add timestamp for each entry
setopt EXTENDED_HISTORY
# removes blank lines from history
setopt HIST_REDUCE_BLANKS

# Case insentive glob
setopt NO_CASE_GLOB
setopt CORRECT_ALL

### Fix slowness of pastes with zsh-syntax-highlighting.zsh
pasteinit() {
  OLD_SELF_INSERT=${${(s.:.)widgets[self-insert]}[2,3]}
  zle -N self-insert url-quote-magic # I wonder if you'd need `.url-quote-magic`?
}

pastefinish() {
  zle -N self-insert $OLD_SELF_INSERT
}
zstyle :bracketed-paste-magic paste-init pasteinit
zstyle :bracketed-paste-magic paste-finish pastefinish
### Fix slowness of pastes

################################################################################
# Configure keyboard
################################################################################
# create a zkbd compatible hash;
# to add other keys to this hash, see: man 5 terminfo
typeset -g -A key

key[Home]="${terminfo[khome]}"
key[End]="${terminfo[kend]}"
key[Insert]="${terminfo[kich1]}"
key[Backspace]="${terminfo[kbs]}"
key[Delete]="${terminfo[kdch1]}"
key[Up]="${terminfo[kcuu1]}"
key[Down]="${terminfo[kcud1]}"
key[Left]="${terminfo[kcub1]}"
key[Right]="${terminfo[kcuf1]}"
key[PageUp]="${terminfo[kpp]}"
key[PageDown]="${terminfo[knp]}"
key[Shift-Tab]="${terminfo[kcbt]}"
key[Control-Left]="${terminfo[kLFT5]}"
key[Control-Right]="${terminfo[kRIT5]}"

# setup key accordingly
[[ -n "${key[Home]}"          ]] && bindkey -- "${key[Home]}"          beginning-of-line
[[ -n "${key[End]}"           ]] && bindkey -- "${key[End]}"           end-of-line
[[ -n "${key[Insert]}"        ]] && bindkey -- "${key[Insert]}"        overwrite-mode
[[ -n "${key[Backspace]}"     ]] && bindkey -- "${key[Backspace]}"     backward-delete-char
[[ -n "${key[Delete]}"        ]] && bindkey -- "${key[Delete]}"        delete-char
[[ -n "${key[Up]}"            ]] && bindkey -- "${key[Up]}"            up-line-or-history
[[ -n "${key[Down]}"          ]] && bindkey -- "${key[Down]}"          down-line-or-history
[[ -n "${key[Left]}"          ]] && bindkey -- "${key[Left]}"          backward-char
[[ -n "${key[Control-Left]}"  ]] && bindkey -- "${key[Control-Left]}"  backward-word
[[ -n "${key[Right]}"         ]] && bindkey -- "${key[Right]}"         forward-char
[[ -n "${key[Control-Right]}" ]] && bindkey -- "${key[Control-Right]}" forward-word
[[ -n "${key[PageUp]}"        ]] && bindkey -- "${key[PageUp]}"        beginning-of-buffer-or-history
[[ -n "${key[PageDown]}"      ]] && bindkey -- "${key[PageDown]}"      end-of-buffer-or-history
[[ -n "${key[Shift-Tab]}"     ]] && bindkey -- "${key[Shift-Tab]}"     reverse-menu-complete

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.
if (( ${+terminfo[smkx]} && ${+terminfo[rmkx]} )); then
    autoload -Uz add-zle-hook-widget
    function zle_application_mode_start { echoti smkx }
    function zle_application_mode_stop { echoti rmkx }
    add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
    add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
fi

# Shortcut to exit shell on partial command line
exit_zsh() { exit }
zle -N exit_zsh
bindkey '^D' exit_zsh

################################################################################


################################################################################
# FZF
################################################################################

# Use fd (https://github.com/sharkdp/fd) instead of the default find
# command for listing path candidates.
# - The first argument to the function ($1) is the base path to start traversal
# - See the source code (completion.{bash,zsh}) for the details.
_fzf_compgen_path() {
  fd --hidden --follow --exclude ".git" . "$1"
}

# Use fd to generate the list for directory completion
_fzf_compgen_dir() {
  fd --type d --hidden --follow --exclude ".git" . "$1"
}

################################################################################

# User configuration

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

###############
### Aliases ###
###############

# ls
alias ls='lsd'
alias l='ls'
alias ll='ls -l'
alias la='ls -a'
alias lll='ls -al'

alias cp='/usr/bin/cp --verbose --interactive'
alias mv='/usr/bin/mv --verbose --interactive'
alias rm='/usr/bin/rm --verbose --interactive=once'

alias vim='nvim'
alias vi='nvim'
alias enw='emacsclient --alternate-editor="" --tty'

alias open='xdg-open'
alias now='date +%FT%T%z'
alias today='date +%F'
alias stripcolors='sed "s/\x1B\[\([0-9]\{1,2\}\(;[0-9]\{1,2\}\)\?\)\?[mGK]//g"'

alias g='git'
alias delete-merged-branches="git branch --merged | grep -v -E '\*|master|develop' | xargs git branch -d 2>>/dev/null || echo 'No branches to remove'"

alias arq='/home/rfonseca/workspace/arquivei/arq/arq_venv/bin/python3 /home/rfonseca/workspace/arquivei/arq/main.py'
alias xclip='/usr/bin/xclip -selection c'

alias t='tmux'
alias tls='tmux ls'
alias ta='tmux new-session -A -s'

alias drun='docker run --rm -it --volume "$(pwd):/mnt" --publish "9999:8000" --network "winterfell" --workdir="/mnt" --user="$(id -u)"'
alias dco='docker-compose'
alias dcoverride='ln -snf $(fd --type f --no-ignore "docker-compose\..*\.ya?ml" | fzf --height=10) docker-compose.override.yaml'
alias dcup='dco up -d'
alias dcstop='dco stop'
alias dcdown='dco down'

alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

alias kcomposectx='ln -snf $(fd --type d --max-depth 1 --hidden "\.kcompose_.*" ~ | fzf --height=10) ~/.kcompose'

alias icat="kitty +kitten icat"
alias diff="kitty +kitten diff"

alias ssh="TERM=xterm-256color /usr/bin/ssh"

alias urldecode='python3 -c "import sys, urllib.parse as ul; print(ul.unquote_plus(sys.argv[1]))"'
alias urlencode='python3 -c "import sys, urllib.parse as ul; print (ul.quote_plus(sys.argv[1]))"'

SOURCELIST=(
  ~/.p10k.zsh
  ~/.google-cloud-sdk/path.zsh.inc
  ~/.google-cloud-sdk/completion.zsh.inc
  /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
  /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
  /usr/share/zsh/plugins/zsh-z/zsh-z.plugin.zsh
#  /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme
  /usr/share/fzf/key-bindings.zsh
  /usr/share/fzf/completion.zsh
)

for f in $SOURCELIST; do
    [ -r $f ] && PATH= source $f
done

if [[ "$TERM" == xterm-kitty ]]; then
  kitty + complete setup zsh | source /dev/stdin
fi

zstyle ':completion:*' menu select

eval "$(starship init zsh)"
